#!/bin/bash

cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat system/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/priv-app/Velvet/Velvet.apk
rm -f system/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/app/Kindle-kfa-release/Kindle-kfa-release.apk.* 2>/dev/null >> system/app/Kindle-kfa-release/Kindle-kfa-release.apk
rm -f system/app/Kindle-kfa-release/Kindle-kfa-release.apk.* 2>/dev/null
cat system/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> system/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f system/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat system/app/YouTube/YouTube.apk.* 2>/dev/null >> system/app/YouTube/YouTube.apk
rm -f system/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/app/Chrome/Chrome.apk.* 2>/dev/null >> system/app/Chrome/Chrome.apk
rm -f system/app/Chrome/Chrome.apk.* 2>/dev/null
