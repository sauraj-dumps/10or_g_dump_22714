## G-user 8.1.0 OPM1.171019.019 10or_G_V1_0_82 release-keys
- Manufacturer: 10or
- Platform: msm8953
- Codename: G
- Brand: 10or
- Flavor: zql1590-user
- Release Version: 8.1.0
- Id: OPM1.171019.019
- Incremental: 10or_G_V1_0_82
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-IN
- Screen Density: undefined
- Fingerprint: 10or/G/G:8.1.0/OPM1.171019.019/10or_G_V1_0_82:user/release-keys
- OTA version: 
- Branch: G-user-8.1.0-OPM1.171019.019-10or_G_V1_0_82-release-keys
- Repo: 10or_g_dump_22714


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
